(function($) {
    var comments = [
    	{
            comment: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            posted: '31.08.2014 20:15',
            author: {
                name: 'John',
                age: 20
            }
        },
    	{
            comment: 'Mauris elit dui, imperdiet vel dui a, aliquet pulvinar nunc.',
            posted: '31.08.2014 19:15',
            author: {
                name: 'Sofia',
                age: 25
            }
        },
    	{
            comment: 'Nulla facilisi.',
            posted: '31.08.2014 19:05',
            author: {
                name: 'Jack',
                age: 15
            }
        },
    	{
            comment: 'Morbi convallis sapien velit, a dignissim risus molestie vitae.',
            posted: '31.08.2014 19:00',
            author: {
                name: 'Fernando',
                age: 50
            }
        }
    ];
    
    $.mockjax({
        url: 'api/comments',
        dataType: 'json',
        contentType: 'application/json',
        type: 'GET',
        responseTime: 1000,
        response: function() {
            this.responseText = comments;
        }
	});
    
    var users = [
    	{
            name: 'John',
            age: 20
        },
    	{
            name: 'Sofia',
            age: 25
        },
    	{
            name: 'Jack',
            age: 15
        },
    	{
            name: 'Rachel',
            age: 32
        },
    	{
            name: 'Fernando',
            age: 50
        },
    	{
            name: 'Isabella',
            age: 45
        }
	];
    
    $.mockjax({
        url: 'api/users',
        dataType: 'json',
        contentType: 'application/json',
        type: 'GET',
        response: function() {
            this.responseText = users;
        }
	});
    
})(jQuery);