define([
	'models/user'
], function(User) {
    return kendo.data.Model.define({
        id: 'id',
        fields: {
            comment: {
                type: 'string'
            },
            posted: {
                type: 'string'
            },
            author: User
        }
    });
});