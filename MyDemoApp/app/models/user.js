define([], function() {
    return kendo.data.Model.define({
        id: 'id',
        fields: {
            name: {
                type: 'string',
                validation: {
                    required: true
                }
            },
            age: {
                type: 'number',
                validation: {
                    min: 1
                }
            }
        }
    });
});