define([
    'uiHelper',
    'text!views/main.html',
	'models/comment'
], function(uiHelper, template, Comment) {

   var model = {
       comments: new kendo.data.DataSource({
           transport: {
                read: {
                    url: 'api/comments',
                    dataType: 'json'
                }
            },
            
			pageSize: 10,
            serverPaging: true,
            schema: {
                model: Comment
            }
        })
   };
   
   uiHelper.InitModule('main', template, model);
});