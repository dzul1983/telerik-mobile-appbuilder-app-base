define([
	'controllers/mainController'
], function() {
    var APP = window.APP = window.APP || {};
    
    return {
        init: function() {
            APP.instance = new kendo.mobile.Application(document.body, {
                initial: 'main'
            });
        }
    };
});