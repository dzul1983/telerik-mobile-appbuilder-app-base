define([
   'models/comment'
], function(Comment) {
   return {
       dataSource: new kendo.data.DataSource({
           transport: {
                read: {
                    url: 'api/comments',
                    dataType: 'json'
                }
            },
            
			pageSize: 10,
            serverPaging: true,
            schema: {
                model: Comment
            }
        })
	};
});