define([
	'models/user'
], function(User) {
    return {
       dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: 'api/users',
                    dataType: 'json'
                }
            },
    		pageSize: 10,
    		serverPaging: true,
            schema: {
                model: Comment
            }
       })
    };
});