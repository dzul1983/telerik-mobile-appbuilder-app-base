define([],
       function() {
           var app = window.APP = window.APP || {};

           return {
               InitModule: function(name, template, model, events) {
                   var baseView = kendo.Class.extend({
                                                         init: function() {
                                                             $(template).appendTo(document.body);
                                                             app[name] = {
                                                                 model: model || {},
                                                                 events: events || {}
                                                             };
                                                         }
                                                     });

                   return new baseView(name, template, model, events);
               },

               CreateLayout: function (name, template, model, events) {
                   $(template).appendTo(document.body);

                   return null;
               }
           };
       });